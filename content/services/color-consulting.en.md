+++
title = "Colour Consulting"
+++

# Colour design
Colour is very individual. Some prefer lots of strong colours, others fine, subtle ones.
You want to express your personality with the colours of the house? The house should fit into the surroundings and meet the requirements of the building authorities. That doesn't have to be a contradiction. I would be happy to help you with the colours and materials of the individual components.
You want to add your personal touch to the interior of your new apartment or family home? I would be happy to work with you to select the colours and materials for the walls, ceilings and suitable floor coverings. The proportions of rooms can be visually designed using colour and brightness.

{{
  galery(name="farbe", pictures=[
    "/057.jpeg",
    "/058.jpeg",
    "/059.jpeg",
    "/060.JPG",
    "/061.jpeg",
    "/062.jpeg",
    "/063.JPG",
    "/064.jpg",
    "/065.JPG",
   ])
}}

# Lighting design
Without light, colours are not visible.
Light is also important for finding our way around buildings, at work, for hobbies, for creating moods, for highlighting images and objects that are important to us... Light is also important in our outdoor spaces, for example for safety, using lighting to extend paths to entrances or creating a romantic garden with atmospheric lighting. I offer a bespoke service where I take the time to walk through the rooms with you, to find out what is important to you and what specific requirements you have, and then to come up with a proposal for the 'right' light (whether... real or on plans) that is unique to you.

{{
  galery(name="licht", pictures=[
    "/066.jpeg",
    "/067.JPG",
    "/068.JPG",
    "/069.JPG",
    "/070.jpeg",
    "/071.jpeg",
    "/072.jpeg",
   ])
}}

# Advice
I also offer individual services separately, in the general area of architecture and interior design with personalized service. You decide the scope of the work that you require. Sub-items such as: room concept with costs, bank dossier, building application, or other services for the successful construction of your new or to be renovated home, as well as colour advice.
Self-builders receive individual advice on which materials and construction methods make sense for the house and which ones suit your abilities.
