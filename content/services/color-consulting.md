+++
title = "Farb Beratung"
+++

# Farbgestaltung

Farbe ist sehr individuell. Manche bevorzugen viele starke Farben andere eher feine, dezente.
Sie möchten Ihre Persönlichkeit mit den Farben des Hauses zum Ausdruck bringen. Das Haus soll sich in die Umgebung einpassen, sowie die Anforderungen der Baubehörden erfüllen. Das muss kein Widerspruch sein. Gerne helfe ich Ihnen dabei die Farben und Materialeren der einzelnen Bauteile dazu.
Sie möchten dem Interieur Ihrer neuen Wohnung oder Ihres Einfamilienhauses Ihre persönliche Note verleihen. Gerne erarbeite ich gemeinsam mit Ihnen die Auswahl der Farben und Materiealien für die Wände, Decken, den passenden Bodenbeläge. Je nach Farbe und Helligkeit lassen sich die Proportionen von Räumen optische gestalten.

{{
  galery(name="farbe", pictures=[
    "/057.jpeg",
    "/058.jpeg",
    "/059.jpeg",
    "/060.JPG",
    "/061.jpeg",
    "/062.jpeg",
    "/063.JPG",
    "/064.jpg",
    "/065.JPG",
   ])
}}

# Lichtgestaltung
Ohne Licht sind Farben nicht sichtbar. 
Licht ist auch wichtig, um uns in Gebäuden zurechtzufinden, bei der Arbeit, bei Hobbys, um Stimmungen zu erzeugen, um Bilder und Objekte hervorzuheben, die uns wichtig sind ... 
Auch in unseren Außenräumen ist Licht wichtig, beispielsweise um die Sicherheit durch Beleuchtung zu erhöhen Erweitern Sie Wege zu Eingängen oder schaffen Sie mit stimmungsvoller 
Beleuchtung einen romantischen Garten. Ich biete einen maßgeschneiderten Service an, bei dem ich mir die Zeit nehme, mit Ihnen durch die Räume zu gehen, herauszufinden, 
was Ihnen wichtig ist und welche besonderen Anforderungen Sie haben, um dann einen Vorschlag für das „richtige“ Licht zu unterbreiten (ob real oder auf Plänen), das ist einzigartig für Sie.

{{
  galery(name="licht", pictures=[
    "/066.jpeg",
    "/067.JPG",
    "/068.JPG",
    "/069.JPG",
    "/070.jpeg",
    "/071.jpeg",
    "/072.jpeg",
   ])
}}

# Beratung
Ich biete einzelne Dienstleistungen separat an, im allgemeinen Bereich der Architektur und Innenarchitektur an mit personalisiertem Service. 
Sie entscheiden, was und welche Arbeiten ich für Sie erledige. Teilpositionen wie: Raumkonzept mit Kosten, Bankdossier, Baueingabe, oder andere Dienstleistungen zum erfolgreichen 
Bau Ihres neuen oder zu erneuernden Heimes, sowie Farbberatung.
Selbstbauer werden individuell beraten, welche Materialien und Bauweisen sinnvoll sind für ihr Haus und welche ihren Fähigkeiten entsprechen.

