+++
title = "Architektur"
+++

Mir ist die persönliche Zusammenarbeit wichtig, darum lade ich Sie zu ich einem unverbindlichen Kennenlernen ein.

# Architektur / Innenarchitektur
Ich biete die gesamten Architekturleistungen mit Innenarchitektur aus einer Hand. Architektur- und Raumkonzept, Ausbau sowie Detailgestaltung bilden eine Einheit. 
Die Anforderungen des Kunden in Bezug auf Design, Qualität und Kosten werden im Laufe der Arbeit in diesen Prozess einbezogen. Dazu gehört auch die Hilfe bei der Wahl des Gebäudestils, der Materialien und der Farbe im Innen- und Außenbereich.
Ich unterstütze Sie bei der Suche des Grundstückes oder des Gebäudes entsprechend Ihren Wünschen und Bedürfnissen und analysieren allfällige Probleme.

## Neubau
Am Anfang an sehe ich mir mit Ihnen das Grundstück an, um das Terrain, Orientierung, Sonnenstand und Schattenwurf, Sicht vom und zu dem zu erstellenden Gebäude zu erfassen und ebenso das Maximum des Möglichen zu erreichen.
Ihre Wünsche nach Platz, Gestaltung und Lebensstil werden einbezogen. Ich führe Sie durch den ganzen Bauprozess.
Meine Erfahrungen im Umgang mit Behörden, Ämtern, Bauvorschriften und Handwerkern kommen Ihnen zu Gute.

{{ galery(name="neubau", pictures=["/006.jpeg", 
  "/007.jpeg", 
  "/008.jpeg", 
  "/009.jpeg", 
  "/010.jpeg", 
  "/011.jpeg", 
  "/012.jpeg", 
  "/013.jpeg", 
  "/014.jpeg", 
  "/015.jpeg", 
  "/016.jpeg", 
  "/017.jpeg", 
  "/018.jpeg", 
  "/019.jpeg", 
  "/020.jpeg", 
  "/021.jpeg", 
  "/022.jpeg", 
  "/023.jpeg", 
  "/024.jpeg", 
  "/025.jpeg", 
  "/025_1.jpg",
  "/026.jpeg", 
  "/027.jpeg",
]) }}

## Um- und Anbau
Wir integrieren Ihre Wünsche und heutige Ansprüche in bestehende Bausubstanz. Bad- und Küchenumbau oder Erweiterungen gehören ebenso zu unserem Repertoire wie Gesamtumbauten.

{{
  galery(name="anbau", pictures=[
    "/028.jpeg",
    "/029.jpeg",
    "/030.jpeg",
    "/031.jpeg",
    "/032.jpg",
    "/032_1.jpg",
  ])
}}

## Renovation und Sanierungen
Ältere Häuser genügen den heutigen Ansprüchen oft nicht mehr. Die Einbauten und Verkleidungen werden entfernt bis auf die Tragkonstruktion um neu ausgebaut zu werden, je nach Zustand das ursprüngliche saniert oder neu erstellt. Steht das Gebäude unter Schutz verhandele ich mit den Ämtern und Behörden, allenfalls Denkmalpflege, um Ihre Bedürfnisse ins Haus zu integrieren.
Wo möglich verwende ich traditionelle Bauweisen und Material zusammen mit neuem, damit modernem Leben zusammen mit dem Charme des bestehenden ins Haus einziehen kann.

{{
  galery(name="anbau", pictures=[
    "/033.jpg",
    "/034.jpg",
    "/035.jpg",
    "/036.jpg",
    "/037.jpg",
    "/038.jpg",
    "/039.JPG",
    "/040.JPG",
    "/041.jpg",
    "/042.jpg",
    "/043.JPG",
    "/044.jpg",
    "/045.JPG",
    "/046.JPG",
    "/047.JPG",
    ])
}}

# Innenarchitektur
Wo hört Architektur auf, wo beginnt Innenarchitektur? Der Übergang ist fliessend. Beides gehört zusammen, es wird auch zusammen wahrgenommen.

# Bühnenbau

{{
  galery(name="buehnenbau", pictures=[
  "/048.JPG",
  "/049.jpg",
  "/050.JPG",
  "/051.JPG",
  "/052.JPG",
  "/053.jpg",
  "/054.jpeg",
  "/055.jpg",
  "/056.jpg",
  ])
}}