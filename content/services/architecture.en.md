+++
title = "Architecture"
+++

Mir ist die persönliche Zusammenarbeit wichtig, darum lade ich Sie zu ich einem unverbindlichen Kennenlernen ein.

# Architecture
I treat both Architecture and Interior Design as one and include them together as one service, combining all stages from design concept, layout, fittings, and details into one unit.
Clients requirements in terms of design, quality and cost are embodied into this process as work develops. Help in choice of style of building, materials and colour both internally and externally is part of this.
I will support you in finding the right property or building according to your wishes and needs and will identify any problems.

## New building
First of all, I will view the property with you in order to the terrain, orientation, position of the sun and shadows, 
view from and to the building to be built and also to achieve the maximum of what is possible. Your wishes for space, design and lifestyle are taken into account. 
I will guide you through the entire construction process. My experience in dealing with authorities, offices, building regulations and tradesmen will benefit you.

{{ galery(name="neubau", pictures=["/006.jpeg", 
  "/007.jpeg", 
  "/008.jpeg", 
  "/009.jpeg", 
  "/010.jpeg", 
  "/011.jpeg", 
  "/012.jpeg", 
  "/013.jpeg", 
  "/014.jpeg", 
  "/015.jpeg", 
  "/016.jpeg", 
  "/017.jpeg", 
  "/018.jpeg", 
  "/019.jpeg", 
  "/020.jpeg", 
  "/021.jpeg", 
  "/022.jpeg", 
  "/023.jpeg", 
  "/024.jpeg", 
  "/025.jpeg", 
  "/025_1.jpg",
  "/026.jpeg", 
  "/027.jpeg",
]) }}

## Alterations and extensions
We will integrate your wishes and current requirements into the existing building structure. 
Bathroom and kitchen alterations or extensions are just as much a part of our repertoire as overall renovations.

{{
  galery(name="anbau", pictures=[
    "/028.jpeg",
    "/029.jpeg",
    "/030.jpeg",
    "/031.jpeg",
    "/032.jpg",
    "/032_1.jpg",
  ])
}}

## Renovation and refurbishment
Older houses often no longer meet today's requirements. The fixtures and cladding are removed except for the supporting structure in order to be rebuilt, 
renovated or restored, depending on the condition of the original one. If the building is listed, I negotiate with the relevant offices and authorities and if necessary, 
with the relevant officials for the conservation of historic monuments, in order to integrate your needs with those of the house.
Where possible I use traditional construction methods and materials together with new ones, so that modern life can co-exist with the charm of the existing house.

{{
  galery(name="anbau", pictures=[
    "/033.jpg",
    "/034.jpg",
    "/035.jpg",
    "/036.jpg",
    "/037.jpg",
    "/038.jpg",
    "/039.JPG",
    "/040.JPG",
    "/041.jpg",
    "/042.jpg",
    "/043.JPG",
    "/044.jpg",
    "/045.JPG",
    "/046.JPG",
    "/047.JPG",
    ])
}}

# Interior design
Where does architecture end and interior design begin? The transition is fluid. Both belong together and are also perceived together.

# Theater design

{{
  galery(name="buehnenbau", pictures=[
    "/048.JPG",
    "/049.jpg",
    "/050.JPG",
    "/051.JPG",
    "/052.JPG",
    "/053.jpg",
    "/054.jpeg",
    "/055.jpg",
    "/056.jpg",
  ])
}}