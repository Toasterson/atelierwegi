+++
title = "Über uns"
+++

# Doris Wegmüller
Dipl. Innenarchitektin FFI, Basel, mit Grundausbildung Hochbauzeichnerin

Architektur und Innenarchitekturbüro in Niederweningen seit 1989

- Diplom CAS Immobilienbewertung
- Mitglied SIV (Schweizerischer Immobilienschätzer-Verband)
- Farbstrategie in der Architektur / Haus der Farbe
- Farbentscheide im öffentlichen Raum / Haus der Farbe
- Farbe in Kunst und Architektur / Haus der Farbe
- Le Corbusier Grand Gamme / KT color
- Lichtgestaltung ZHAW
- und mehr

Sprachen: Deutsch und Englisch

Tätigkeitsbereich:
- Neubau von EFH und MFH
- An- und Umbau von EFH und MFH
- Holzelementbau
- Renovation und Sanierung von Bauernhäusern, Verhandlungen mit Denkmalpflege
- Innenarchitektur, EFH, Läden, Restaurants, Dancings
- Farb-  + Lichtgestaltung
- Wintergärten
- Gebäude- und Kostenschätzungen

{{ right_side_image(path="/profile.jpg", width=300, height=400, op="fit_width") }}