+++
title = "About Us"
+++

# Doris Wegmüller
Dipl. Interior Design, Basel. Cert. Architectural Technology, Winterthur

Architecture and Interior Design office opened in 1989, Niederweningen

- CAS in Real Estate Valuation
- Member of SIV (Association of Swiss Real Estate Valuers)
- Colour strategy in architecture / Haus der Farbe
- Colour decisions in public spaces / Haus der Farbe
- Colour in art and architecture / Haus der Farbe
- Colour Le Corbusier Grand Gamme / KT color 
- Lighting design ZHAW
- and more

Languages:  German and English

My fields of activity:
- New build single and multiple family houses
- Alterations and extensions to single and multiple family houses
- Renovation and refurbishment of complete buildings including farm houses and listed buildings.
- Interior design for housing, shops, restaurants and night clubs
- Material, light and colour schemes and advice
- Conservatories
- Building and Cost Estimating

{{ right_side_image(path="/profile.jpg", width=300, height=400, op="fit_width") }}