+++
title = "Atelier Wegi"
+++

# Welcome to our homepage

You don’t know how high you can fly until you have tried.

I assist my clients personally from land/building search until the finished building.

Architecture and interior design are at one with the surroundings.

{{
  resize_image(path="/001.jpg", width=600, height=400, op="fit")
}}

{{
  galery(name="farbe", pictures=[
    "/002.jpg",
    "/003.JPG",
    "/004.JPG",
    "/005.jpg",
   ])
}}
