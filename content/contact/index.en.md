+++
title = "Contact"
template = "contact.html"
+++
# E-Mail

For further information please contact us by E-mail or telephone. For larger or complex inquiries, use the telephone, this will save you and us misunderstandings and time.

[atelierwegi@gmail.com](mailto:atelierwegi@gmail.com)

[Tel 044 856 17 64](tel:+41448561764)

# How To Find Us
**Adresse:** <br>
Im Uwerd 5 <br>
8166 Niederweningen

<div id="map"></div>
<script>
  var map = L.map('map').setView([47.50982, 8.37986], 15);
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);
var marker = L.marker([47.51061, 8.37924]).addTo(map);
</script>
