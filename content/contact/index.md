+++
title = "Kontakt"
template = "contact.html"
+++
# E-Mail

Für kleinere Anfragen, können Sie untenstehende E-Mail Adresse verwenden. Für grössere oder komplexe Anfragen benutzen Sie lieber das Telefon, dies erspart Ihnen und uns Missverständnisse und Zeit.

[atelierwegi@gmail.com](mailto:atelierwegi@gmail.com)

[Tel 044 856 17 64](tel:+41448561764)

# Anreise
**Adresse:** <br>
Im Uwerd 5 <br>
8166 Niederweningen

<div id="map"></div>
<script>
  var map = L.map('map').setView([47.50982, 8.37986], 15);
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);
var marker = L.marker([47.51061, 8.37924]).addTo(map);
</script>
