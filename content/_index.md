+++
title = "Homepage"
+++

# Willkommen auf unserer Homepage

Du weißt nicht wie hoch Du fliegen kannst, bevor Du es versuchst
 
Ich begleiten unsere Kunden von der Land-/Haussuche bis zum Einzug persönlich.

Architektur und Innenarchitektur bilden zusammen mit der Umgebung eine Einheit.

{{ 
  resize_image(path="/001.jpg", width=600, height=400, op="fit") 
}}

{{
  galery(name="farbe", pictures=[
    "/002.jpg",
    "/003.JPG",
    "/004.JPG",
    "/005.jpg",
   ])
}}
